var accountList = ["xxx1", "xxx2", "xxx3", "xxx4", "xxx5", "xxx6", "xxx7", "xxx8", "xxx9", "xxx10", "xxx11", "xxx12", "xxx13", "xxx14", "xxx15"];
//console.log(accountList);
var checkbox = "<div class='item'>";
var text = "";
var offset = 0;
var range = 3;
var dataSize = accountList.length;
var seemore = document.getElementById("seemore");
var selectAccountList = [];

$(document).ready(function() {
	var check = 0;
	for(var accountItem in accountList){
		if(accountList[accountItem].productGroup == 'ออมทรัพย์' || accountList[accountItem].productGroup == 'กระแสรายวัน'){
			check += 1
		}
	}
	if(check == 0){
		window.location.href = BaseUrl + "thankYou";
	}else{
		ListAccount();
	}	
});

// List Loop
function ListAccount() {
    count = offset + range;
    if (count >= dataSize) {
        count = dataSize;
        seemore.style.visibility = "hidden";
    }
    for (var i = offset; i < count; i++) {
    	console.log(accountList[i])
        text += checkbox + "<label><input type='checkbox' onclick='selectAccount(" + i + ")'"
        		+ "<span class='checkbox-text'>" + "&nbsp;" + accountList[i].productGroup + "&nbsp;" + accountList[i].accountNo 
        		+ "<br>&nbsp;สาขา.........." 
        		+ "<br>&nbsp;" + accountList[i].balAmt + "&nbsp;บาท"
        		+ "</span></label></div>";
        //console.log(accountList[i].accountNo);
    }
    //console.log(text);
    $('#listAccounts').html(text);
    offset = offset + range;
}
function seemoreAccounts() {
    range = 10;
    ListAccount();
    //console.log(offset);
}

function selectAccount(accountIndex){
	//console.log(accountIndex);
	var checkPush = 0;
	var accountChoose = [];
	for(var accountItem in selectAccountList){
		if(selectAccountList[accountItem] == accountIndex){
			checkPush += 1;
		}else{
			accountChoose.push(selectAccountList[accountItem]);
		}
	}
	selectAccountList = accountChoose;
	if(checkPush == 0){
		selectAccountList.push("" + accountIndex);
	}
	console.log(selectAccountList);
}

function sendSaveChooseAccount(){
	var dataAccountSelect = [];
	//console.log(accountList);
	for(index in selectAccountList){
		dataAccountSelect.push(accountList[selectAccountList[index]]);
	}
	console.log(dataAccountSelect)
	if(dataAccountSelect.length === 0){
		swal({
			  title: "ยืนยันบริการแจ้งเตือน",
			  text: "ท่านไม่เลือกบัญชีเพื่อรับบริการแจ้งเตือน Krungthai Connext (สามารถทำการเลือกบัญชีได้อีกครั้งในภายหลัง ที่เมนูจัดการข้อมูล)",
			  type: "warning",
			  showCancelButton: true,
			  confirmButtonClass: "btn-info",
			  cancelButtonClass: "btn-info",			  
			  confirmButtonText: "ย้อนกลับ",
			  cancelButtonText: "ยืนยัน",
			  allowOutsideClick: false
			}).then(function(isConfirm){
				if(isConfirm.value == undefined){
					console.log()
					saveChooseAccount(dataAccountSelect);
				}				
			});
	}else{
		saveChooseAccount(dataAccountSelect);
	}	
}
function saveChooseAccount(dataAccountSelect){
	$.ajax({
		url : BaseUrl + "saveChooseAccount",
		type : 'post',
		data : JSON.stringify({
			"inquiryListAccountInfoRec" : dataAccountSelect,
		}),
		contentType : "application/json",
		success : function(response) {
			console.log(response);
			swal({
		           title: "ทำรายการสำเร็จ",
		           text: response.data,
		           type: "success",
		           allowOutsideClick: false
			});
		},
		error : function(){
			swal({
		           title: "ไม่สามารถทำรายการได้",
		           text: "เนื่องจากพบข้อผิดพลาดบางอย่าง โปรดใช้บริการใหม่ในภายหลัง",
		           type: "error",
		           allowOutsideClick: false
			});
		}
	});
}
