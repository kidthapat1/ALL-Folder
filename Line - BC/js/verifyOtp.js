/**
 * 
 */
function isNumberKey(evt)
{
var otpnumber = document.getElementById("otpNumber").value;
var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57)){
			return false;
         }
		 if(otpnumber.length == 5){
	document.getElementById("submitVerifyOtp").disabled = false;
	document.getElementById('errOTP').innerHTML = '';
		 return true;
		 }
}

function isBackspace(evt)
{
var otpnumber = document.getElementById("otpNumber").value;
var charCode = (evt.which) ? evt.which : event.keyCode
if (charCode == 8 ){
	if(otpnumber.length >= 7){	
	document.getElementById('errOTP').innerHTML = '';
	document.getElementById("submitVerifyOtp").disabled = false;
	}else{
	document.getElementById("submitVerifyOtp").disabled = true;
	}
}

}

function chkOTPNum(){

var chkOTPNum; 
var otpnumber = document.getElementById("otpNumber").value;

if(otpnumber.length>0){
	if(otpnumber.length < 6){	
	document.getElementById('errOTP').innerHTML = 'รหัส OTP ไม่ถูกต้อง';
	}else{
	document.getElementById('errOTP').innerHTML = '';
	}
}
}

function checkOTP() {
	var otpNumber = $("#otpNumber").val();
	var otpRef = $("#refOtp").val();	
	//console.log("OTP : " + otpNumber + ",OtpRef : " + otpRef);	
	$.ajax({
		url : BaseUrl + "verifyOtp",
		type : 'post',
		data : JSON.stringify({
			"otpNumber" : otpNumber,
			"refOtp" : otpRef
		}),
		contentType : "application/json",
		success : function(response) {
			console.log(response);
			//window.location.href = BaseUrl + "accountAlert";
			if(response.status == "SUCCESS"){
				window.location.href = BaseUrl + "accountAlert";
			}else if(response.status == "ERROR"){
				clear();
				swal({
					  title: response.message.title,
					  text: response.message.message,
					  type: "warning",
					  showCancelButton: false,
					  confirmButtonClass: "btn-info",
					  confirmButtonText: "Ok!",
					  allowOutsideClick: false
					}).then(function(isConfirm){
						if(response.message.message == "OTP is lock."){
							window.location.href = BaseUrl + "register";
						}
					});				
			}
		},
		error : function (error){
			console.log(error);
		}
	});
}

function clear(){
	$("#otpNumber").val("");
	document.getElementById("submitVerifyOtp").disabled = true;
}