/**
 * 
 */

$(document).ready(function() {
	openTab(event, 'Thai');
	if(sessionStorage.getItem("identityValue") != undefined){
		if(sessionStorage.getItem("identityType") == "CID"){
			openTab(event, 'Thai');
			$('#citizenID').val(sessionStorage.getItem("identityValue"));
			$('#mobilePhoneTH').val(sessionStorage.getItem("mobilePhone"));
			chkComplate();
		}else{
			openTab(event, 'Foreigner');
			$('#passport').val(sessionStorage.getItem("identityValue"));
			$('#mobilePhoneEN').val(sessionStorage.getItem("mobilePhone"));
			chkForeigner();
		}
	}
});

function clear(){
	$('#citizenID').val("");
	$('#passport').val("");
	$('#mobilePhoneTH').val("");
	$('#mobilePhoneEN').val("");
	document.getElementById('errCID').innerHTML = '';
	document.getElementById('errMobile').innerHTML = '';
	document.getElementById('errMobileEn').innerHTML = '';
	document.getElementById('errPassport').innerHTML = '';
	document.getElementById("submitBtn").disabled = true;
}

function openTab(evt, name) {
	clear();
	var i, tabcontent;
	tabcontent = document.getElementsByClassName("tabcontent");
	for (i = 0; i < tabcontent.length; i++) {
		tabcontent[i].style.display = "none";
	}
	document.getElementById(name).style.display = "block";
}

function isNumberKey(evt) {
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
	return true;
}

function chkComplate() {
	var chkMobile = false;
	var chkDigit = false;
	var cid = document.getElementById("citizenID").value;
	var mobile = document.getElementById("mobilePhoneTH").value;
	if (cid.length == 13) {
		
		for (i = 0, sum = 0; i < 12; i++) {
				sum += parseFloat(cid.charAt(i)) * (13 - i);
				if ((11 - sum % 11) % 10 != parseFloat(cid.charAt(12))) {
					//console.log(parseFloat(cid.charAt(12)));
					chkDigit = false;
				} else {
					chkDigit = true;
				}
			}
		

		if (chkDigit != true) {
			document.getElementById('errCID').innerHTML = 'เลขที่บัตรประชาชนไม่ถูกต้อง';
		} else {
			document.getElementById('errCID').innerHTML = '';
		}
	}	

	
		if (mobile.charAt(0) != "0" && mobile.length > 0) {
			document.getElementById('errMobile').innerHTML = 'เบอร์โทรไม่ถูกต้อง';
			chkMobile = false;
		} else if(mobile.length == 10){
			document.getElementById('errMobile').innerHTML = '';
			chkMobile = true;
		}else{
			document.getElementById('errMobile').innerHTML = '';
			
		}
	
	if (chkDigit === false || chkMobile === false) {
		document.getElementById("submitBtn").disabled = true;
	} else {
		document.getElementById("submitBtn").disabled = false;
	}
}

function chkForeigner() {

	var chkMobile;
	var passport = document.getElementById("passport").value;
	var mobile = document.getElementById("mobilePhoneEN").value;

	if (mobile.charAt(0) != "0" && mobile.length > 0) {
		document.getElementById('errMobileEn').innerHTML = 'Mobile Phone was wrong';
		chkMobile = false;
	} else if(mobile.length == 10){
		document.getElementById('errMobileEn').innerHTML = '';
		chkMobile = true;
	}else{
		document.getElementById('errMobileEn').innerHTML = '';		
	}
	if (passport.length > 0 && chkMobile == true) {
		document.getElementById("submitBtn").disabled = false;
	} else {
		document.getElementById("submitBtn").disabled = true;
	}

}

function checkRegister() {
	var identityValue = "";
	var mobilePhone = "";
	var identityType = "";
	if ($('#citizenID').val() !== "") {
		identityValue = $('#citizenID').val()
		identityType = "CID";
	} else {
		identityValue = $('#passport').val()
		identityType = "PP";
	}
	if ($('#mobilePhoneTH').val() !== "") {
		mobilePhone = $('#mobilePhoneTH').val()
	} else {
		mobilePhone = $('#mobilePhoneEN').val()
	}
	sessionStorage.setItem("identityValue",identityValue);
	sessionStorage.setItem("mobilePhone",mobilePhone);
	sessionStorage.setItem("identityType",identityType);
	$.ajax({
		url : BaseUrl + "checkRegister",
		type : 'post',
		data : JSON.stringify({
			"identityValue" : identityValue,
			"mobilePhone" : mobilePhone,
			"identityType" : identityType
		}),
		contentType : "application/json",
		success : function(response) {
			console.log(response);
			//window.location.href = BaseUrl + "requestOtp";
			if(response.status == "SUCCESS"){
				window.location.href = BaseUrl + "requestOtp";
			}else{
				swal({
			           title: response.message.title,
			           text: response.message.message,
			           type: "warning",
			           allowOutsideClick: false
				});
			}
		},
		error : function(){			
			swal({
		           title: "ไม่สามารถทำรายการได้",
		           text: "เนื่องจากพบข้อผิดพลาดบางอย่าง โปรดใช้บริการใหม่ในภายหลัง",
		           type: "error",
		           allowOutsideClick: false
			});
		}
	});
}