/**
 * 
 */

function openTCTab(evt, name) {
	var i, tabTermAndCondition;
	tabTermAndCondition = document
			.getElementsByClassName("tabTermAndCondition");
	for (i = 0; i < tabTermAndCondition.length; i++) {
		tabTermAndCondition[i].style.display = "none";

	}
	document.getElementById(name).style.display = "block";

	document.getElementById("checkAcceptEng").checked = false
	document.getElementById("checkAcceptThai").checked = false
	document.getElementById("acceptButtonEng").disabled = true;
	document.getElementById("acceptButtonThai").disabled = true;

	setSelectedLanguage(name);
}

function setSelectedLanguage(lang) {
	sessionStorage.setItem('languageType', lang);
}

function checkboxFunction(checkName, buttonName) {
	var checkBox = document.getElementById(checkName);
	var text = document.getElementById(buttonName);
	if (checkBox.checked == true) {
		document.getElementById(buttonName).disabled = false;

	} else {
		document.getElementById(buttonName).disabled = true;
	}
}

function onAccepted() {
	
	$.ajax({
		url : BaseUrl + "termAndCondition",
		type : 'post',
		contentType : "application/json",
		success : function(response) {
			window.location.href = BaseUrl + "register";
		}
	});
		
}