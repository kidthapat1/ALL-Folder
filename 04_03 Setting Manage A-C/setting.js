/**
 * 
 */
var language;
$(document).ready(function() {
	checkRegister();	
});

function checkRegister(){
	$.ajax({
		url : BaseUrl + "checkRegister",
		type : "post",
		contentType : "application/json",
		success : function(response) {
			if(response.status === "SUCCESS"){
				language = response.data.identifierType;
				if(language === "PP"){	
					sessionStorage.setItem("languageType","EN");	
					document.getElementById("title").innerHTML = "Setting";
					document.getElementById("mngAcc").innerHTML = "&emsp;Transaction notifications";
					document.getElementById("cancel").innerHTML = "&emsp;Cancel Krungthai Connext";
				}else{
					sessionStorage.setItem("languageType","TH");	
					document.getElementById("title").innerHTML = "ตั้งค่า";
					document.getElementById("mngAcc").innerHTML = "&emsp;การแจ้งเตือนบัญชี";		
					document.getElementById("cancel").innerHTML = "&emsp;ยกเลิกบริการ Krungthai Connext";
				}	
			}else{
				$.ajax({
					url : BaseUrl + "getErrorException?message=ERROR_CONFIRM_REGISTER",
					type : 'get',
					contentType : "application/json",
					success : function(response) {							
							swal({
							           /*title: response.message.title,*/
									   html: response.message.message + "</br><small>" + response.messageEn.message + "</small>",
							           /*type: "warning",*/
									   imageUrl: BaseUrl + 'images/ic-alert@3x.png',
									   imageWidth: 50,
									   imageHeight: 50,
							           confirmButtonText : "สมัคร/<small>Register</small>",
							           confirmButtonClass: 'btn btn-primary',
							           buttonsStyling: false,
							           allowOutsideClick: false
							}).then(function(isConfirm){
								if(isConfirm.value == true){
									window.location.href = BaseUrlRegister;
								}						
							});						
						}
					});					
			}
		}
	});
}