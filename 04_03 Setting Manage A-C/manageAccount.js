/**
 * 
 */

var accountList = [];
/*console.log(accountList);*/
var defAccountList = [];
var text = "";
var setNoti = "";
var offset = 0;
var range = 3;
var dataSize = accountList.length;
var selectAccountList = [];
var opendiv = "<div class='item'>";
var closediv = "</div>";
var opendivDATA = "<div>";
var closeddivDATA = "</div>";
var opendivtoggle = "<div class='toggleSW'>";
var closedivtoogle = "</div>";

var selectAccountList;
$(document).ready(function() {
	getAccountList();
});

function getAccountList() {
	$.ajax({
		url : BaseUrl + "getAccountList",
		type : "post",
		contentType : "application/json",
		success : function(response) {
			console.log(response);
			accountList = response.data;
			ListAccount();
			Language();
		},
		error : function() {
			$.ajax({
				url : BaseUrl + "getErrorException?message=ERROR_EXCEPTION",
				type : 'get',
				contentType : "application/json",
				success : function(response) {
					if (language === "TH") {
						swal({
							title : response.message.title,
							text : response.message.message,
							imageUrl : BaseUrl + 'images/ic-alert@3x.png',
							imageWidth : 50,
							imageHeight : 50,
							confirmButtonText : "ตกลง",
							confirmButtonClass : 'btn btn-primary',
							buttonsStyling : false,
							allowOutsideClick : false
						});
					} else {
						swal({
							title : response.messageEn.title,
							text : response.messageEn.message,
							imageUrl : BaseUrl + 'images/ic-alert@3x.png',
							imageWidth : 50,
							imageHeight : 50,
							confirmButtonText : "OK",
							confirmButtonClass : 'btn btn-primary',
							buttonsStyling : false,
							allowOutsideClick : false
						});
					}
				}
			});
		}
	});

}



function ListAccount() {
	var lang = sessionStorage.getItem("languageType");
	if (lang === "EN") {
		for (var i = 0; i < accountList.length; i++) {
			if (accountList[i].noti == "1") {
				defAccountList.push(i);
				selectAccount(i, true);
				setNoti = "checked";
			} else {
				setNoti = "";
			}
			console.log(accountList[i]);

			text += opendiv
					+ opendivDATA
					+ accountList[i].productGroupEN
					+ "&nbsp;"
					+ accountList[i].accountNoTxt
					+ "<br>"
					+ "Branch.........."
					+ "<br>"
					+ accountList[i].balAmt
					+ "&nbsp;THB"
					+ closeddivDATA
					+ opendivtoggle
					+ "<label class='switch containerCBox text-right' id='selectAllTxt'><input class='checkmark' type='checkbox' id='checkAcceptEng' onclick='selectAccount("
					+ i + ",this)' " + setNoti
					+ "><span class='slider round'></span></label>"
					+ closedivtoogle + closediv;
		}
	} else {

		for (var i = 0; i < accountList.length; i++) {
			if (accountList[i].noti == "1") {
				defAccountList.push(i);
				selectAccount(i, true);
				setNoti = "checked";
			} else {
				setNoti = "";
			}
			console.log(accountList[i]);

			text += opendiv
					+ opendivDATA
					+ accountList[i].productGroupTH
					+ "&nbsp;"
					+ accountList[i].accountNoTxt
					+ "<br>"
					+ "สาขา.........."
					+ "<br>"
					+ accountList[i].balAmt
					+ "&nbsp;บาท"
					+ closeddivDATA
					+ opendivtoggle
					+ "<label class='switch containerCBox text-right' id='selectAllTxt'><input class='checkmark' type='checkbox' id='checkAcceptEng' onclick='selectAccount("
					+ i + ",this)' " + setNoti
					+ "><span class='slider round'></span></label>"
					+ closedivtoogle + closediv;
		}

	}
	$("#listAccounts").html(text);
	offset = offset + range;
}

/*function onChangeSelectAccount(index) {
	var oldAlertFlag = selectAccountList[index].notificationFlag
	selectAccountList[index].notificationFlag = changeFlag(oldAlertFlag)
	var oldChangFlag = selectAccountList[index].onChangeFlag
	selectAccountList[index].onChangeFlag = changeFlag(oldChangFlag)
	console.log(selectAccountList);
}

function changeFlag(flag) {
	var flagnew = "";
	if (flag === "Y") {
		flagnew = "N";
	} else if (flag === "N") {
		flagnew = "Y";
	}
	return flagnew;
}

function saveChooseAccount(accountList) {
	$.ajax({
		url : BaseUrl + "saveChooseAccount",
		type : "post",
		data : JSON.stringify({
			inquiryListAccountInfoRec : dataAccountSelect
		}),
		contentType : "application/json",
		success : function(response) {
			console.log(response);
			$.ajax({
				url : BaseUrl + "getErrorException?message=ERROR_SUCCESS",
				type : 'get',
				contentType : "application/json",
				success : function(response) {
					if (language === "TH") {
						swal({
							title : response.message.title,
							text : response.message.message,
							type : "success",
							confirmButtonText : "ตกลง",
							confirmButtonClass : 'btn btn-primary',
							buttonsStyling : false,
							allowOutsideClick : false
						});
					} else {
						swal({
							title : response.messageEn.title,
							text : response.messageEn.message,
							type : "success",
							confirmButtonText : "OK",
							confirmButtonClass : 'btn btn-primary',
							buttonsStyling : false,
							allowOutsideClick : false
						});
					}
				}
			});
		},
		error : function() {
			$.ajax({
				url : BaseUrl + "getErrorException?message=ERROR_EXCEPTION",
				type : 'get',
				contentType : "application/json",
				success : function(response) {
					if (language === "TH") {
						swal({
							title : response.message.title,
							text : response.message.message,
							imageUrl : BaseUrl + 'images/ic-alert@3x.png',
							imageWidth : 50,
							imageHeight : 50,
							confirmButtonText : "ตกลง",
							confirmButtonClass : 'btn btn-primary',
							buttonsStyling : false,
							allowOutsideClick : false
						});
					} else {
						swal({
							title : response.messageEn.title,
							text : response.messageEn.message,
							imageUrl : BaseUrl + 'images/ic-alert@3x.png',
							imageWidth : 50,
							imageHeight : 50,
							confirmButtonText : "OK",
							confirmButtonClass : 'btn btn-primary',
							buttonsStyling : false,
							allowOutsideClick : false
						});
					}
				}
			});
		}
	});
}*/

function selectAccount(accountIndex,checkbox) {
	
	  var checkPush = 0;
	  var accountChoose = [];
	  for (var accountItem in selectAccountList) {
	    if (selectAccountList[accountItem] == accountIndex) {
	      checkPush += 1;
	    } else {
	      accountChoose.push(selectAccountList[accountItem]);
	    }
	  }
	  selectAccountList = accountChoose;
	  if (checkPush == 0) {
	    selectAccountList.push("" + accountIndex);
	  }

	  if (selectAccountList.length === dataSize) {
	    $("#checkbox")[0].checked = true;
	  } else {
	    $("#checkbox")[0].checked = false;
	  }
	  if (checkPush == 0) {
	    $(checkbox).parent().parent().parent().addClass("isSelected");
	  }else{
	    $(checkbox).parent().parent().parent().removeClass("isSelected");
	  }
	  
	  checkActionChange();
	  
	}


function checkActionChange() {
	//var x = document.getElementById("myCheck").checked;
	var chkChange = false;
	
	if(defAccountList.length > 0 && selectAccountList.length > 0){	
				
		if(selectAccountList.length > defAccountList.length ){
			for (var i = 0; i < selectAccountList.length; i++) {
			if( Search_Array( defAccountList , selectAccountList[i] ) )
			{				
				chkChange = chkChange||false
			}
			else
			{				
				chkChange = chkChange||true
			}		
		}
			
		}else{
			for (var i = 0; i < defAccountList.length; i++) {
			if( Search_Array( selectAccountList , defAccountList[i] ) )
			{				
				chkChange = chkChange||false
			}
			else
			{				
				chkChange = chkChange||true
			}		
		}			
			
		}			
		
		
		
	}else if(defAccountList.length > 0 && selectAccountList.length == 0){
		chkChange = true;		
	}else if(selectAccountList.length > 0){
		chkChange = true;		
	}
	

	


	if(chkChange == true){document.getElementById("acceptButtonEng").disabled = false;}
	else{document.getElementById("acceptButtonEng").disabled = true;}
	
}

function Search_Array(ArrayObj, SearchFor)
{
for (var i = 0; i < ArrayObj.length; i++)
{
  if (ArrayObj[i] == SearchFor) return true ; 
}

return false ;
}

function sendSaveChooseAccount() {
	var language = sessionStorage.getItem("languageType");
	var dataAccountSelect = [];
	for (index in selectAccountList) {
		dataAccountSelect.push(accountList[selectAccountList[index]]);
	}
	console.log(dataAccountSelect);
	if (dataAccountSelect.length === 0) {
		$.ajax({
			url : BaseUrl + "getErrorException?message=ERROR_NOT_CHOOSE",
			type : 'get',
			contentType : "application/json",
			success : function(response) {
					if(language === "TH"){
						swal({
					           title: response.message.title,
					           text: response.message.message,
					           type: "warning",
							   showCancelButton : true,
							   confirmButtonClass : "btn btn-primary",
							   cancelButtonClass : "btn btn-primary-cancel",
							   confirmButtonText : "ย้อนกลับ",
							   cancelButtonText : "ยืนยัน",
					           buttonsStyling: false,
					           allowOutsideClick: false
						});
					}else{
						swal({
					           title: response.messageEn.title,
					           text: response.messageEn.message,
					           type: "warning",
					           showCancelButton : true,
							   confirmButtonClass : "btn btn-primary",
							   cancelButtonClass : "btn btn-primary-cancel",
							   confirmButtonText : "Back",
							   cancelButtonText : "Confirm",
					           buttonsStyling: false,
					           allowOutsideClick: false
						});
					}		
				}
			});			
		
	} else {
		saveChooseAccount(dataAccountSelect);
	}
}
function saveChooseAccount(dataAccountSelect) {
	var language = sessionStorage.getItem("languageType");
			$.ajax({
				url : BaseUrl + "saveChooseAccount",
				type : "post",
				data : JSON.stringify({
					inquiryListAccountInfoRec : dataAccountSelect
				}),
				contentType : "application/json",
				success : function(response) {
					console.log(response);
					$.ajax({
						url : BaseUrl + "getErrorException?message=ERROR_SUCCESS",
						type : 'get',
						contentType : "application/json",
						success : function(response) {
								if(language === "TH"){
									swal({
								           title: response.message.title,
								           text: response.message.message,
								           type: "success",
								           confirmButtonText : "ตกลง",
								           confirmButtonClass: 'btn btn-primary',
								           buttonsStyling: false,
								           allowOutsideClick: false
									});
								}else{
									swal({
								           title: response.messageEn.title,
								           text: response.messageEn.message,
								           type: "success",
								           confirmButtonText : "OK",
								           confirmButtonClass: 'btn btn-primary',
								           buttonsStyling: false,
								           allowOutsideClick: false
									});
								}		
							}
						});
				},
				error : function() {
					$.ajax({
						url : BaseUrl + "getErrorException?message=ERROR_EXCEPTION",
						type : 'get',
						contentType : "application/json",
						success : function(response) {
								if(language === "TH"){
									swal({
								           title: response.message.title,
								           text: response.message.message,
								           type: "error",
								           confirmButtonText : "ตกลง",
								           confirmButtonClass: 'btn btn-primary',
								           buttonsStyling: false,
								           allowOutsideClick: false
									});
								}else{
									swal({
								           title: response.messageEn.title,
								           text: response.messageEn.message,
								           type: "error",
								           confirmButtonText : "OK",
								           confirmButtonClass: 'btn btn-primary',
								           buttonsStyling: false,
								           allowOutsideClick: false
									});
								}		
							}
						});		
				}
			});
}

function getSelectedLanguage() {
	return sessionStorage.getItem("languageType");
}

function Language() {
	var lang = getSelectedLanguage();
	console.log(accountList);
	var library = {
		title : {
			th : "เปิด-ปิดการแจ้งเตือน",
			en : "On-off notifications"
		},
		subtitle : {
			th : "เพื่อรับบริการแจ้งเตือนผ่าน Krungthai Connext เฉพาะบัญชีออมทรัพย์และบัญชีกระแสรายวันเท่านั้น",
			en : " Select Saving and current accounts to receive transaction notifications via Krungthai Connext"
		},
		acceptButton : {
			th : "ยืนยัน",
			en : "Confirm"
		},
		selectAllTxt : {
			th : "เลือกทั้งหมด",
			en : "Select all"
		},
		amountAccountTxt : {
			th : "บัญชี",
			en : "A/C"
		}
	};

	var title = $("#title");
	var subtitle = $("#subtitle");
	var acceptButton = $("#acceptButton");
	var selectAllTxt = $("#selectAllTxt");
	var amountAccountTxt = $("#amountAccountTxt");

	if (lang === "EN") {

		title[0].innerText = library["title"].en;
		subtitle[0].innerText = library["subtitle"].en;
		amountAccountTxt[0].innerText = library["selectAllTxt"].en + " "
				+ accountList.length + " " + library["amountAccountTxt"].en;
		acceptButton[0].innerText = library["acceptButton"].en;

	} else {
		title[0].innerText = library["title"].th;
		subtitle[0].innerText = library["subtitle"].th;
		amountAccountTxt[0].innerText = library["selectAllTxt"].th + " "
				+ accountList.length + " " + library["amountAccountTxt"].th;
		acceptButton[0].innerText = library["acceptButton"].th;

	}
}

function selectAll(source) {
	var item = document.querySelectorAll('.item');
	if (selectAccountList.length !== accountList.length) {
		selectAccountList = [];
		$(item).addClass("isSelected");
	} else {
		$(item).removeClass("isSelected");
	}

	for (var i = 0; i < accountList.length; i++) {
		selectAccount(i);
	}
	var checkboxes = document.querySelectorAll('input[type="checkbox"]');
	for (var i = 0; i < checkboxes.length; i++) {
		if (checkboxes[i] != source)
			checkboxes[i].checked = source.checked;
	}
	checkActionChange()
}