*** Settings ***
Suite Setup    Open Browser    https://www.katalon.com/    firefox
Suite Teardown    Close Browser
Resource    seleniumLibrary.robot

*** Variables ***
${undefined}    https://www.katalon.com/

*** Test Cases ***
Test Case
    open    https://www.inwc.ktb.co.th/KTB-Line-Register/
    click    xpath=(.//*[normalize-space(text()) and normalize-space(.)='ข้าพเจ้ายอมรับในข้อกำหนดและเงื่อนไข'])[1]/span[1]
    click    xpath=(.//*[normalize-space(text()) and normalize-space(.)='ข้าพเจ้ายอมรับในข้อกำหนดและเงื่อนไข'])[1]/span[1]
    click    xpath=(.//*[normalize-space(text()) and normalize-space(.)='English'])[1]/following::label[1]
    click    xpath=(.//*[normalize-space(text()) and normalize-space(.)='English'])[1]/following::label[1]
    click    xpath=(.//*[normalize-space(text()) and normalize-space(.)='ข้าพเจ้ายอมรับในข้อกำหนดและเงื่อนไข'])[1]/span[1]
    click    id=acceptButtonThai