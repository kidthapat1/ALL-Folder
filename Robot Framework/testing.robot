***TestCases***

Search
    เปิด Link
    กดเงื่อนไข Checkbox
    กดปุ่ม ตกลง เปลี่ยนสี
    กดปุ่มอีกรอบ เปลี่ยนสี
    # กรอกข้อมูลCID
    # กรอกข้อมูลPHONE
    # กดปุ่มถัดไป

# Test Teardown   Close Browser

***Settings***
Library     Selenium2Library

***Variables***
# Browser ที่จะใช้
${BROWSER}      chrome
# Link Browser ที่ใช้
${inwc URL}       https://www.inwc.ktb.co.th/KTB-Line-Register/
# user password
${CID}          1849901439516
${PHONE}        0982305739
***Keywords***
เปิด Link
    Open Browser        ${inwc URL}       ${BROWSER}  
กดเงื่อนไข Checkbox
    Click Element       class:checkmark
กดปุ่มอีกรอบ เปลี่ยนสี
    Click Element       class:checkmark
กดปุ่ม ตกลง เปลี่ยนสี
    Click Button        id:acceptButtonThai
# กรอกข้อมูลCID
#     Input text          citizenID           ${CID}
# กรอกข้อมูลPHONE
#     Input text          id:mobilePhoneTH    ${PHONE}
# กดปุ่มถัดไป
#     Click Button        id:submitBtn