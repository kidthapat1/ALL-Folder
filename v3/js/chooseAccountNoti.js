//var accountList = [
//  "xxx1",
//  "xxx2",
//  "xxx3",
//  "xxx4",
//  "xxx5",
//  "xxx6",
//  "xxx7",
//  "xxx8",
//  "xxx9",
//  "xxx10",
//  "xxx11",
//  "xxx12",
//  "xxx13",
//  "xxx14",
//  "xxx15"
//];
//console.log(accountList);

var text = "";
var offset = 0;
var range = 3;
var dataSize = accountList.length;
var selectAccountList = [];

$(document).ready(function() {
	var check = 0;
	for ( var accountItem in accountList) {
		if (accountList[accountItem].productGroup == "ออมทรัพย์" || accountList[accountItem].productGroup == "กระแสรายวัน") {
				check += 1;
		}
	}
	if (check == 0) {
		window.location.href = BaseUrl + "thankYou";
	} else {
		ListAccount();
		Language();
	}
});

// List Loop
function ListAccount() {
	var lang = getSelectedLanguage();
	if(lang === "TH"){
		for (var i = 0; i < accountList.length; i++) {
			console.log(accountList[i]);
        
			text += "<div class='form-check form-check-inline item'> <label class='label2'> <label class='containerCBox'> <input class='form-check-input checkmark' type='checkbox' id='checkAcceptEng' onclick='selectAccount("+i+",this)'><span class='checkmark cbox'></span> </label>"
					+ "<label class='form-check-label' for='inlineCheckbox1'>"
					+ accountList[i].productGroupTH + "&nbsp;"
					+ accountList[i].accountNoTxt + "<br>"
					+ "สาขา.........." + "<br>"
					+ accountList[i].balAmt + "&nbsp;บาท"
					+ "</label></label></div>";
		}
	}else{
		for (var i = 0; i < accountList.length; i++) {
			console.log(accountList[i]);		
			text += "<div class='form-check form-check-inline item'> <label class='label2'> <label class='containerCBox'> <input class='form-check-input checkmark' type='checkbox' id='checkAcceptEng' onclick='selectAccount("+i+",this)'><span class='checkmark cbox'></span> </label>"
					+ "<label class='form-check-label' for='inlineCheckbox1'>"
					+ accountList[i].productGroupEN + "&nbsp;"
					+ accountList[i].accountNoTxt + "<br>"
					+ "สาขา.........." + "<br>"
					+ accountList[i].balAmt + "&nbsp;THB"
					+ "</label></label></div>";
		}
	}
	$("#listAccounts").html(text);
	offset = offset + range;
}

function selectAccount(accountIndex,checkbox) {
	  var checkPush = 0;
	  var accountChoose = [];
	  for (var accountItem in selectAccountList) {
	    if (selectAccountList[accountItem] == accountIndex) {
	      checkPush += 1;
	    } else {
	      accountChoose.push(selectAccountList[accountItem]);
	    }
	  }
	  selectAccountList = accountChoose;
	  if (checkPush == 0) {
	    selectAccountList.push("" + accountIndex);
	  }

	  if (selectAccountList.length === dataSize) {
	    $("#checkbox")[0].checked = true;
	  } else {
	    $("#checkbox")[0].checked = false;
	  }
	  if (checkPush == 0) {
	    $(checkbox).parent().parent().parent().addClass("isSelected");
	  }else{
	    $(checkbox).parent().parent().parent().removeClass("isSelected");
	  }
	  
	}

function sendSaveChooseAccount() {
	var language = sessionStorage.getItem("languageType");
	var dataAccountSelect = [];
	for (index in selectAccountList) {
		dataAccountSelect.push(accountList[selectAccountList[index]]);
	}
	console.log(dataAccountSelect);
	if (dataAccountSelect.length === 0) {
		$.ajax({
			url : BaseUrl + "getErrorException?message=ERROR_NOT_CHOOSE",
			type : 'get',
			contentType : "application/json",
			success : function(response) {
					if(language === "TH"){
						swal({
					           title: response.message.title,
					           text: response.message.message,
					           type: "warning",
							   showCancelButton : true,
							   confirmButtonClass : "btn btn-primary",
							   cancelButtonClass : "btn btn-primary-cancel",
							   confirmButtonText : "ย้อนกลับ",
							   cancelButtonText : "ยืนยัน",
					           buttonsStyling: false,
					           allowOutsideClick: false
						});
					}else{
						swal({
					           title: response.messageEn.title,
					           text: response.messageEn.message,
					           type: "warning",
					           showCancelButton : true,
							   confirmButtonClass : "btn btn-primary",
							   cancelButtonClass : "btn btn-primary-cancel",
							   confirmButtonText : "Back",
							   cancelButtonText : "Confirm",
					           buttonsStyling: false,
					           allowOutsideClick: false
						});
					}		
				}
			});			
		
	} else {
		saveChooseAccount(dataAccountSelect);
	}
}
function saveChooseAccount(dataAccountSelect) {
	var language = sessionStorage.getItem("languageType");
			$.ajax({
				url : BaseUrl + "saveChooseAccount",
				type : "post",
				data : JSON.stringify({
					inquiryListAccountInfoRec : dataAccountSelect
				}),
				contentType : "application/json",
				success : function(response) {
					console.log(response);
					$.ajax({
						url : BaseUrl + "getErrorException?message=ERROR_SUCCESS",
						type : 'get',
						contentType : "application/json",
						success : function(response) {
								if(language === "TH"){
									swal({
								           title: response.message.title,
								           text: response.message.message,
								           type: "success",
								           confirmButtonText : "ตกลง",
								           confirmButtonClass: 'btn btn-primary',
								           buttonsStyling: false,
								           allowOutsideClick: false
									});
								}else{
									swal({
								           title: response.messageEn.title,
								           text: response.messageEn.message,
								           type: "success",
								           confirmButtonText : "OK",
								           confirmButtonClass: 'btn btn-primary',
								           buttonsStyling: false,
								           allowOutsideClick: false
									});
								}		
							}
						});
				},
				error : function() {
					$.ajax({
						url : BaseUrl + "getErrorException?message=ERROR_EXCEPTION",
						type : 'get',
						contentType : "application/json",
						success : function(response) {
								if(language === "TH"){
									swal({
								           title: response.message.title,
								           text: response.message.message,
								           type: "error",
								           confirmButtonText : "ตกลง",
								           confirmButtonClass: 'btn btn-primary',
								           buttonsStyling: false,
								           allowOutsideClick: false
									});
								}else{
									swal({
								           title: response.messageEn.title,
								           text: response.messageEn.message,
								           type: "error",
								           confirmButtonText : "OK",
								           confirmButtonClass: 'btn btn-primary',
								           buttonsStyling: false,
								           allowOutsideClick: false
									});
								}		
							}
						});		
				}
			});
}

function getSelectedLanguage() {
	return sessionStorage.getItem("languageType");
}

function Language() {
		var lang = getSelectedLanguage();
		console.log(accountList);
		var library = {
			message1 : {
				th : "เลือกบัญชี",
				en : "Please select accounts"
			},
			message2 : {
				th : "เพื่อรับบริการแจ้งเตือนผ่าน Krungthai Connext",
				en : "to receive transaction notifications via Krungthai Connext"
			},
			message3 : {
				th : "เฉพาะบัญชีออมทรัพย์และกระแสรายวันเท่านั้น",
				en : "Savings and current accounts only"
			},
			acceptButton : {
				th : "ยืนยัน",
				en : "Confirm"
			},
			selectAllTxt : {
				th : "เลือกทั้งหมด",
				en : "Select all"
			},
			amountAccountTxt : {
				th : "บัญชี",
				en : "Accounts"
			}
		};

		var message1 = $("#message1");
		var message2 = $("#message2");
		var message3 = $("#message3");
		var seemore = $("#seemore");
		var acceptButton = $("#acceptButton");
		var selectAllTxt = $("#selectAllTxt");
		var amountAccountTxt = $("#amountAccountTxt");

		if (lang === "TH") {
			message1[0].innerText = library["message1"].th;
			message2[0].innerText = library["message2"].th;
			message3[0].innerText = library["message3"].th;
			selectAllTxt[0].innerText = library["selectAllTxt"].th;
			amountAccountTxt[0].innerText = accountList.length + " " + library["amountAccountTxt"].th;
			acceptButton[0].innerText = library["acceptButton"].th;

		} else if (lang === "EN") {
			message1[0].innerText = library["message1"].en;
			message2[0].innerText = library["message2"].en;
			message3[0].innerText = library["message3"].en;
			selectAllTxt[0].innerText = library["selectAllTxt"].en;
			amountAccountTxt[0].innerText = accountList.length + " " + library["amountAccountTxt"].en;
			acceptButton[0].innerText = library["acceptButton"].en;
		}
}
function selectAll(source) {
	  var item = document.querySelectorAll('.item');
	  if (selectAccountList.length !== accountList.length) {
	    selectAccountList = [];
	    $(item).addClass("isSelected");
	  }else{
	    $(item).removeClass("isSelected");
	  }

	  for (var i = 0; i < accountList.length; i++) {
	    selectAccount(i);
	  }
	  var checkboxes = document.querySelectorAll('input[type="checkbox"]');
	  for (var i = 0; i < checkboxes.length; i++) {
	    if (checkboxes[i] != source) checkboxes[i].checked = source.checked;
	  }
	}
