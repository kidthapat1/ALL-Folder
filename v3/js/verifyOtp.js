/**
 * 
 */
 
 
var language;
 
 $(document).ready(function() {
	 language = sessionStorage.getItem("languageType");	/*("TH","EN")*/	
	 console.log(language);
		if (language === "TH"){
			document.getElementById("otpPageTH").style.display = "block";
		} 
		/*language = "EN";*/
		else { 
			document.getElementById("otpPageEN").style.display = "block";
		}
});


function isNumberKey(evt)
{
var otpnumberth = document.getElementById("otpNumberTH").value;
var otpnumberen = document.getElementById("otpNumberEN").value;
var charCode = (evt.which) ? evt.which : event.keyCode

	if (language === "TH"){
         if (charCode > 31 && (charCode < 48 || charCode > 57)){
			return false;
         }
		 if(otpnumberth.length == 5){
	document.getElementById("submitVerifyOtpTH").disabled = false;
	document.getElementById('errOTPTH').innerHTML = '';
		 return true;
		 }
	}
	else {
         if (charCode > 31 && (charCode < 48 || charCode > 57)){
			return false;
         }
		 if(otpnumberen.length == 5){
	document.getElementById("submitVerifyOtpEN").disabled = false;
	document.getElementById('errOTPEN').innerHTML = '';
		 return true;
		 }
	}
}

function isBackspace(evt)
{
var otpnumberth = document.getElementById("otpNumberTH").value;
var otpnumberen = document.getElementById("otpNumberEN").value;
var charCode = (evt.which) ? evt.which : event.keyCode

	if (language === "TH"){
		if (charCode == 8 ){
			if(otpnumberth.length >= 7){	
			document.getElementById('errOTPTH').innerHTML = '';
			document.getElementById("submitVerifyOtpTH").disabled = false;
			}else{
			document.getElementById("submitVerifyOtpTH").disabled = true;
			}
		}
	}
	else{
		if (charCode == 8 ){
			if(otpnumberen.length >= 7){	
			document.getElementById('errOTPEN').innerHTML = '';
			document.getElementById("submitVerifyOtpEN").disabled = false;
			}else{
			document.getElementById("submitVerifyOtpEN").disabled = true;
			}
		}
	}
}

function chkOTPNum(){

var chkOTPNum; 
var otpnumberth = document.getElementById("otpNumberTH").value;
var otpnumberen = document.getElementById("otpNumberEN").value;


	if (language === "TH"){	
		if(otpnumberth.length>0){
			if(otpnumberth.length < 6){	
			document.getElementById('errOTPTH').innerHTML = '';
			}else{
			document.getElementById('errOTPTH').innerHTML = '';
			}
		}
	}
	else{
		if(otpnumberen.length>0){
			if(otpnumberen.length < 6){	
			document.getElementById('errOTPEN').innerHTML = '';
			}else{
			document.getElementById('errOTPEN').innerHTML = '';
			}
		}
	}
}

function checkOTP() {
	var otpNumber;
	var otpRef;	
	var language = sessionStorage.getItem("languageType");
	if (language === "TH"){
		otpNumber = $("#otpNumberTH").val();
		otpRef = $("#refOtpTH").val();	
	} 
	else { 
		otpNumber = $("#otpNumberEN").val();
		otpRef = $("#refOtpEN").val();	
	}

	//console.log("OTP : " + otpNumber + ",OtpRef : " + otpRef);	
	$.ajax({
		url : BaseUrl + "verifyOtp",
		type : 'post',
		data : JSON.stringify({
			"otpNumber" : otpNumber,
			"refOtp" : otpRef
		}),
		contentType : "application/json",
		success : function(response) {
			//window.location.href = BaseUrl + "accountAlert";
			if(response.status == "SUCCESS"){
				window.location.href = BaseUrl + "accountAlert";
			}else if(response.status == "ERROR"){
				clear();
				if(language === "TH"){
					swal({
						  title: response.message.title,
						  text: response.message.message,
						  type: "warning",
						  confirmButtonText : "ตกลง",
				          confirmButtonClass: 'btn btn-primary',
				          buttonsStyling: false,
				          allowOutsideClick: false
						}).then(function(isConfirm){
							if(response.message.message == "OTP is lock."){
								window.location.href = BaseUrl + "register";
							}
						});	
				}else{
					swal({
						  title: response.messageEn.title,
						  text: response.messageEn.message,
						  type: "warning",
						  confirmButtonText : "OK",
				          confirmButtonClass: 'btn btn-primary',
				          buttonsStyling: false,
				          allowOutsideClick: false
						}).then(function(isConfirm){
							if(response.message.message == "OTP is lock."){
								window.location.href = BaseUrl + "register";
							}
						});	
				}							
			}
		},
		error : function (error){
			$.ajax({
				url : BaseUrl + "getErrorException",
				type : 'get',
				contentType : "application/json",
				success : function(response) {
						if(language === "TH"){
							swal({
						           title: response.message.title,
						           text: response.message.message,
						           type: "error",
						           confirmButtonText : "ตกลง",
						           confirmButtonClass: 'btn btn-primary',
						           buttonsStyling: false,
						           allowOutsideClick: false
							});
						}else{
							swal({
						           title: response.messageEn.title,
						           text: response.messageEn.message,
						           type: "error",
						           confirmButtonText : "OK",
						           confirmButtonClass: 'btn btn-primary',
						           buttonsStyling: false,
						           allowOutsideClick: false
							});
						}		
					}
				});		
		}
	});
	
	
}

function clear(){
	$("#otpNumberTH").val("");
	$("#otpNumberEN").val("");
	document.getElementById("submitVerifyOtpTH").disabled = true;
	document.getElementById("submitVerifyOtpEN").disabled = true;
}