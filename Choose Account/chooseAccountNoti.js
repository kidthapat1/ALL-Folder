var opendiv = "<div class='item'>";
var closediv = "</div>";
var opendivDATA = "<div>";
var closeddivDATA = "</div>";
var opendivtoggle = "<div class='toggleSW'>";
var closedivtoogle = "</div>";
var text = "";
var dataSize = accountList.length;
var selectAccountList = [];
// var offset = 0;
// var range = 3;
// var seemore = document.getElementById("seemore");

$(document).ready(function() {
  var check = 0;
  for (var accountItem in accountList) {
    if (
      accountList[accountItem].productGroup == "ออมทรัพย์" ||
      accountList[accountItem].productGroup == "กระแสรายวัน"
    ) {
      check += 1;
    }
  }
  if (check == 0) {
    window.location.href = BaseUrl + "thankYou";
  } else {
    ListAccount();
    Language();
  }
});
// List Loop Account
function ListAccount() {
  var lang = getSelectedLanguage();
  if (lang === "TH") {
    for (var i = 0; i < accountList.length; i++) {
      console.log(accountList[i]);
      text +=
        opendiv +
        opendivDATA +
        accountList[i].productGroupTH +
        "&nbsp;" +
        accountList[i].accountNoTxt +
        "<br>" +
        "สาขา.........." +
        "<br>" +
        accountList[i].balAmt +
        "&nbsp;บาท" +
        closeddivDATA +
        opendivtoggle +
        "<label class='switch containerCBox text-right'><input class='checkmark' type='checkbox' id='checkAcceptEng' onclick='selectAccount(" +
        i +
        ",this)'><span class='slider round'></span></label>" +
        closedivtoogle +
        closediv;
      // console.log(accountList[i].accountNo);
    }
  } else {
    for (var i = 0; i < accountList.length; i++) {
      console.log(accountList[i]);
      text +=
        opendiv +
        opendivDATA +
        accountList[i].productGroupEN +
        "&nbsp;" +
        accountList[i].accountNoTxt +
        "<br>" +
        "Branch.........." +
        "<br>" +
        accountList[i].balAmt +
        "&nbsp;THB" +
        closeddivDATA +
        opendivtoggle +
        "<label class='switch containerCBox'><input class='checkmark' type='checkbox' id='checkAcceptEng' onclick='selectAccount(" +
        i +
        ",this)'><span class='slider round'></span></label>" +
        closedivtoogle +
        closediv;
      // console.log(accountList[i].accountNo);
    }
  }

  // console.log(text);
  $("#listAccounts").html(text);
  // offset = offset + range;
}

function selectAccount(accountIndex, checkbox) {
  // console.log(accountIndex);
  var checkPush = 0;
  var accountChoose = [];
  for (var accountItem in selectAccountList) {
    if (selectAccountList[accountItem] == accountIndex) {
      checkPush += 1;
    } else {
      accountChoose.push(selectAccountList[accountItem]);
    }
  }
  selectAccountList = accountChoose;
  if (checkPush == 0) {
    selectAccountList.push("" + accountIndex);
  }

  if (selectAccountList.length === dataSize) {
    $("#checkbox")[0].checked = true;
  } else {
    $("#checkbox")[0].checked = false;
  }

  if (checkPush == 0) {
    $(checkbox)
      .parent()
      .parent()
      .parent()
      .addClass("isSelected");
  } else {
    $(checkbox)
      .parent()
      .parent()
      .parent()
      .removeClass("isSelected");
  }
}

function sendSaveChooseAccount() {
  var dataAccountSelect = [];
  // console.log(accountList);
  for (index in selectAccountList) {
    dataAccountSelect.push(accountList[selectAccountList[index]]);
  }
  console.log(dataAccountSelect);
  if (dataAccountSelect.length === 0) {
    swal({
      title: "ยืนยันบริการแจ้งเตือน",
      text:
        "ท่านไม่เลือกบัญชีเพื่อรับบริการแจ้งเตือน Krungthai Connext (สามารถทำการเลือกบัญชีได้อีกครั้งในภายหลัง ที่เมนูจัดการข้อมูล)",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-info",
      cancelButtonClass: "btn-info",
      confirmButtonText: "ย้อนกลับ",
      cancelButtonText: "ยืนยัน",
      allowOutsideClick: false
    }).then(function(isConfirm) {
      if (isConfirm.value == undefined) {
        console.log();
        saveChooseAccount(dataAccountSelect);
      }
    });
  } else {
    saveChooseAccount(dataAccountSelect);
  }
}

function saveChooseAccount(dataAccountSelect) {
  $.ajax({
    url: BaseUrl + "saveChooseAccount",
    type: "post",
    data: JSON.stringify({
      inquiryListAccountInfoRec: dataAccountSelect
    }),
    contentType: "application/json",
    success: function(response) {
      console.log(response);
      swal({
        title: "ทำรายการสำเร็จ",
        text: response.data,
        type: "success",
        allowOutsideClick: false
      });
    },
    error: function() {
      swal({
        title: "ไม่สามารถทำรายการได้",
        text: "เนื่องจากพบข้อผิดพลาดบางอย่าง โปรดใช้บริการใหม่ในภายหลัง",
        type: "error",
        allowOutsideClick: false
      });
    }
  });
}

function getSelectedLanguage() {
  return sessionStorage.getItem("languageType");
}

function Language() {
  var lang = getSelectedLanguage();
  console.log(accountList);
  var library = {
    title: {
      th: "เลือกบัญชี",
      en: "Select Accounts"
    },
    subtitle: {
      th:
        "เพื่อรับบริการแจ้งเตือนผ่าน Krungthai Connext เฉพาะบัญชีออมทรัพย์และบัญชีกระแสรายวันเท่านั้น",
      en:
        " Select Saving and current accounts to receive transaction notifications via Krungthai Connext"
    },
    acceptButton: {
      th: "ยืนยัน",
      en: "Confirm"
    },
    selectAllTxt: {
      th: "เลือกทั้งหมด",
      en: "Select all"
    },
    amountAccountTxt: {
      th: "บัญชี",
      en: "A/C"
    }
  };

  var title = $("#title");
  var subtitle = $("#subtitle");
  var acceptButton = $("#acceptButton");
  var selectAllTxt = $("#selectAllTxt");
  var amountAccountTxt = $("#amountAccountTxt");

  if (lang === "TH") {
    title[0].innerText = library["title"].th;
    subtitle[0].innerText = library["subtitle"].th;
    amountAccountTxt[0].innerText =
      library["selectAllTxt"].th +
      " " +
      accountList.length +
      " " +
      library["amountAccountTxt"].th;
    acceptButton[0].innerText = library["acceptButton"].th;
  } else if (lang === "EN") {
    title[0].innerText = library["title"].en;
    subtitle[0].innerText = library["subtitle"].en;
    amountAccountTxt[0].innerText =
      library["selectAllTxt"].en +
      " " +
      accountList.length +
      " " +
      library["amountAccountTxt"].en;
    acceptButton[0].innerText = library["acceptButton"].en;
  }
}
function selectAll(source) {
  var item = document.querySelectorAll(".item");
  if (selectAccountList.length !== accountList.length) {
    selectAccountList = [];
    $(item).addClass("isSelected");
  } else {
    $(item).removeClass("isSelected");
  }

  for (var i = 0; i < accountList.length; i++) {
    selectAccount(i);
  }
  var checkboxes = document.querySelectorAll('input[type="checkbox"]');
  for (var i = 0; i < checkboxes.length; i++) {
    if (checkboxes[i] != source) checkboxes[i].checked = source.checked;
  }
}
